package pl.aleksandria.items;

import java.util.Objects;

public abstract class Item{


   private String title;
   private String author;
   private TypeOfBook type;
   private String description;
   private int howManyHave;




    public Item(String title, String author, TypeOfBook type, String description, int howManyHave) {
        this.title = title;
        this.author = author;
        this.type = type;
        this.description = description;
        this.howManyHave = howManyHave;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public TypeOfBook getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public int getHowManyHave() {
        return howManyHave;
    }

    public void setHowManyHave(int howManyHave) {
        this.howManyHave = howManyHave;
    }

       @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(title, item.title) &&
                Objects.equals(author, item.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, type);
    }

    @Override
    public String toString() {
        return "{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", type=" + type +
                '}';
    }
}


