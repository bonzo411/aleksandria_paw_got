package pl.aleksandria.items;

public enum TypeOfBook {

    FANTASY,
    DRAMA;
}
