package pl.aleksandria.items;

public class Book extends  Item {

    private String numberOfPages;


    public Book(String title, String author, TypeOfBook type, String description, int howManyHave, String numberOfPages) {
        super(title, author, type, description, howManyHave);
        this.numberOfPages = numberOfPages;
    }

    public String getNumberOfPages() {
        return numberOfPages;
    }






}
