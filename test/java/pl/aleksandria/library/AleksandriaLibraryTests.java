package pl.aleksandria.library;

import pl.aleksandria.items.Audiobook;
import pl.aleksandria.items.Book;
import pl.aleksandria.items.TypeOfBook;
import org.junit.Assert;
import org.junit.Test;

public class AleksandriaLibraryTests {

    @Test
    public void addItemToList_sizeOfList1(){
        AleksandriaLibrary lista = new AleksandriaLibrary();
     final int size = 1;

     lista.addItem(new Book("as","paw", TypeOfBook.DRAMA,"dobrakniga",4,"12" ));
     final int actual = lista.getItemsInLibrary().size();

        Assert.assertEquals(size, actual);

    }


    @Test
    public void addItem_twoThisSameItem_size1AndhowManyHave2(){
        AleksandriaLibrary lista = new AleksandriaLibrary();
        Audiobook element = new Audiobook("as","paw",TypeOfBook.DRAMA,"dobrakniga",1,"12");
        Audiobook element1 = new Audiobook("as","paw",TypeOfBook.DRAMA,"dobrakniga",1,"12");

        lista.addItem(element);
        lista.addItem(element1);

         final int exceptedHowManyHave = 2;
         final int exceptedSize = 1;

         final int actualSize = lista.getItemsInLibrary().size();
         final int actualHowManyHave = element.getHowManyHave();

         Assert.assertEquals(exceptedSize,actualSize);
         Assert.assertEquals(exceptedHowManyHave,actualHowManyHave);


    }

@Test
    public void removeItem_removeThisSameItem_Howmanyhave1_sizeOflist1(){
        AleksandriaLibrary lista = new AleksandriaLibrary();
        Audiobook element1 = new Audiobook("as","paw",TypeOfBook.DRAMA,"dobrakniga",2,"12");
         lista.addItem(element1);

         final int expected = 1;
         final int expectedHowManyHave = 1;

         boolean wynik = lista.removeItem("as","paw",Audiobook.class);
         final int actualHowManyHave = element1.getHowManyHave();
         final int actualSizeOfList = lista.getItemsInLibrary().size();

         Assert.assertEquals(expectedHowManyHave,actualHowManyHave);
         Assert.assertEquals(expected,actualSizeOfList);
}




}
